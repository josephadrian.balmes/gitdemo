import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GraphPagePage } from './graph-page.page';

const routes: Routes = [
  {
    path: '',
    component: GraphPagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GraphPagePageRoutingModule {}
