import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GraphPagePageRoutingModule } from './graph-page-routing.module';

import { GraphPagePage } from './graph-page.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GraphPagePageRoutingModule
  ],
  declarations: [GraphPagePage]
})
export class GraphPagePageModule {}
