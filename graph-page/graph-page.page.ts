import { Component, OnInit} from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-graph-page',
  templateUrl: './graph-page.page.html',
  styleUrls: ['./graph-page.page.scss'],
})
export class GraphPagePage implements OnInit {
 
  public billE: string=""
  public homeE: string=""
  public personalE: string=""
   public tuitionE: string=""
  constructor(private router: Router) { 

    this.homeE=localStorage.getItem('EHome')
    this.personalE=localStorage.getItem('EPersonal')
    this.billE=localStorage.getItem('Ebill')
    this.tuitionE=localStorage.getItem('ETuition')

  }

  reset(){

    localStorage.setItem('ETuition', `0`)
    localStorage.setItem('Ebill', `0`)
    localStorage.setItem('EPersonal', `0`)
    localStorage.setItem('EHome', `0`)
    this.router.navigate(['/home'])

  }
  
  ngOnInit() {

   
    
  }

}
