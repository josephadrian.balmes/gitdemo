import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: '',
    redirectTo: 'start-screen',
    pathMatch: 'full'
  },
  {
    path: 'profile',
    loadChildren: () => import('./profile/profile.module').then( m => m.ProfilePageModule)
  },
  {
    path: 'graph-page',
    loadChildren: () => import('./graph-page/graph-page.module').then( m => m.GraphPagePageModule)
  },
  {
    path: 'budget-page',
    loadChildren: () => import('./budget-page/budget-page.module').then( m => m.BudgetPagePageModule)
  },
  {
    path: 'start-screen',
    loadChildren: () => import('./start-screen/start-screen.module').then( m => m.StartScreenPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
